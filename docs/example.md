# 實作

## header 回應標頭

=== "禁止緩存，每次請求都需要與伺服器驗證資源"

    ```js
    res.setHeader('Cache-Control', 'no-cache');
    ```

=== "禁止緩存並存儲於本地緩存，強制要求用戶端不要將相應緩存或存儲"

    ```js
    res.setHeader('Cache-Control', 'no-store');
    ```

=== "設置快取最大存活時間為 40 秒，超過這個時間後，用戶端必須重新向伺服器驗證資源是否有效"

    ```js
    res.setHeader('Cache-Control', 'max-age=40');
    ```

---

=== "保持連接"

    ```js
    res.setHeader('Connection', 'keep-alive');
    ```

=== "完成回應後立即關閉連接"

    ```js
    res.setHeader('Connection', 'close');
    ```

---

```title="Server 伺服器的名字"
Server: Apache/2.4.1 (Unix)
```

---

## header 請求標頭

```js title="Accept 指定客戶端能夠接收的內容類型"
const acceptHeader = req.headers['accept'];

if (acceptHeader.includes('text/html')) {
  res.setHeader('Content-Type', 'text/html');
  res.send('<h1>Hello</h1>');
} else if (acceptHeader.includes('application/json')) {
  res.setHeader('Content-Type', 'application/json');
  res.send({ message: 'Hello' });
} else {
  res.status(406).send( 'Not Acceptable');
}
```

Accept-Encoding: 指定客戶端能夠接受的內容編碼方式，例如 gzip、deflate 等，用於壓縮傳輸的內容。  

當請求來自客戶端時，系統會先檢查緩存中是否有相應的資源。如果在緩存中找不到資源，則將向伺服器發送請求，從伺服器獲取響應並將其存入緩存中。下次再請求相同的資源時，將從緩存中返回，而無需再次向伺服器發送請求。

這帶來了幾個好處：首先是更快的響應時間，因為不需要向伺服器發送請求；其次是減少伺服器帶寬的使用，因為響應來自緩存而不是伺服器；最後是減輕了伺服器的負載，因為不是所有請求都需要到達伺服器。

Authorization: 用來向伺服器做身份認證（authentication）的憑證（credentials）  

Host: 指明了請求將要傳送到的伺服器主機名稱和連接埠號  

Referer: 表示瀏覽器所訪問的前一個頁面  
User-Agent: 伺服器軟體的名稱和版本號

---

## [Node.js] cookie-session驗證原理以及express-session套件使用

我們前面講過，HTTP 是無狀態的，所以server並不會保存前一次request的任何資訊。也就是說，如果不做任何額外的設定的話，如果我今天登入了某個網站，只要按了重新整理(再發一次get request)，這個網站就會要求我重新登入一次，因為server已經不記得我了。

### 如何保存使用者狀態

解決這個問題，我們要讓client request附上一個「憑證」，這樣server端才有辦法記住已經登入過的人，讓使用者不用一直無限的重新登入。所以，可以想像client端和server端都需要有個儲存空間來存放這個憑證，才能做出驗證/登入的功能。

```title="透過cookie-session驗證使用者流程如下"
  使用者登入 -> 驗證資料庫內的帳號密碼 -> 通過後在伺服器端把使用者資訊存入session store，並生成session ID作為索引-> 給使用者session ID存放在瀏覽器的cookie -> 下次使用者進入頁面時，比對cookie內的session ID和伺服器端一不一樣 -> 如果一樣則回傳使用者資訊，不一樣則不讓使用者進入頁面。
```

### 使用express-session實作

```title="引入express-session套件"
npm install express-session
```

```js title="express-session是一個middleware，需要把它寫在路由之前"
const session = require('express-session')
app.use(session({
    secret: 'mySecret', //用來簽名存放在cookie的sessionID
    name: 'user', // 存放在cookie的key，如果不寫的話預設是connect.sid
    saveUninitialized: false, //會把「還沒修改過的」session就存進session store
    resave: true, //如果不想要session被清理掉的話，就要把這個設定為true
  })
)
// 後面再寫路由...
```

把express-session寫在路由之前，所有的request都會生成一個session並可以透過req.session這個變數來取得session內容，以及req.sessionID來取得session ID。

```title="可以在後端用console log來觀察這些變數"
app.get('/', (req, res) => {
  console.log(req.session)
  console.log(req.sessionID) 
})
```

```title="重新看一下驗證的流程"
使用者登入 -> 驗證資料庫內的帳號密碼 -> 通過後在伺服器端把使用者資訊存入session store，並生成session ID作為索引-> 給使用者session ID存放在瀏覽器的cookie -> 下次使用者進入頁面時，比對cookie內的session ID和伺服器端一不一樣 -> 如果一樣則回傳使用者資訊，不一樣則不讓使用者進入頁面。
```

```title="打開瀏覽器developer tool即可看到一組key-value pair在cookie裡面"
s%3AdsQLFGN-LvWJCOIzjuAmTbsXdo_OjdS8.Tt%2B0PlQsInBo%2F6qleNxvunRAIcL9JpFQ2Y5ljzaR55c
```

比對一下，server端存的req.sessionID的確和cookie存放的session ID是符合的

---

## (實作)用JWT取代傳統Session來驗證使用者身份

JWT 是一個很長的 base64 字串在這字串中分為三個部分別用點號來分隔

- Header: 裡面分別儲存型態和加密方法，通常系統是預設 HS256 雜湊演算法來加密
- Payload: 它和 Session 一樣，可以把一些自定義的數據存儲在 Payload 裡例如像是用戶資料
- Signature: 做為檢查碼是為了預防前兩部分被中間人偽照修改或利用的機制

### 開始

``` js title="首先建立一個 payload 內容為你要傳遞的資料"
const payload = {
  user_name: username
};
```

``` js title="利用 jsonwebtoken 組件 來產生一個 Token 囉！我們使用 jwt.sign() 來取得 Token"
const token = jwt.sign(payload, process.env.JWT_SECRET, { expiresIn: '15m' });
// Payload + 狀態資料
// 使用 expiresIn(exp) 這是設定 Token 的時效性
// 簽署金鑰，這個金鑰是保密的也只存放在後端不能前端用戶知道，API 驗證才會使用到
```

``` js title="最後產生token"
res.status(200).json({ message: 'Login successful', username, token });
```

``` json title="回傳結果"
{
    "message": "Login successful",
    "username": "leo3",
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiJsZW8zIiwiaWF0IjoxNzEzNTQ2ODIyLCJleHAiOjE3MTM1NDc3MjJ9.zokmUbwyjJXYYawGkzcrjxhqF5jktglh7TNYga80x3k"
}
```
