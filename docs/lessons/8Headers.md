# 常用的Headers

---

通用（General）：

- Request URL：請求的URL地址，指示客戶端要訪問的資源。例如，<https://www.example.com/page.html。>
- Request Method：請求方法，例如GET、POST、PUT等，指示伺服器應該如何處理請求。例如，GET用於獲取資源，POST用於提交資料到伺服器。
- Status Code：狀態碼，用於指示伺服器的回應狀態，例如200表示成功，404表示未找到等。例如，200 OK、404 Not Found。
- Remote Address：客戶端的IP位址。例如，192.168.1.1。
- Referrer Policy：指示瀏覽器應該如何處理引用頁面的策略，用於保護隱私和安全。例如，strict-origin-when-cross-origin。
回應（Response）：

- Server：伺服器軟體的名稱和版本號，指示正在使用的伺服器技術。例如，Apache/2.4.29 (Ubuntu)。
- Set-Cookie：用於在回應中設置Cookie，通常在伺服器希望在客戶端保存一些狀態信息時使用。例如，sessionid=abc123; Path=/; Expires=Wed, 09 Jun 2021 10:18:14 GMT。
- Content-Type：指定HTTP主體的媒體類型，通常在回應中使用，告訴客戶端發送的資料類型。例如，text/html表示HTML文檔，application/json表示JSON數據。
- Content-Length：發送主體的長度，指示客戶端接收到的請求資料的大小。例如，Content-Length: 1024。
- Date：回應的日期和時間。例如，Wed, 09 Jun 2021 10:18:14 GMT。
請求（Request）：

- User-Agent：發送請求的用戶端的標識信息，通常是瀏覽器或其他客戶端應用程式的名稱和版本號。例如，User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36。
- Referer：引用頁面的URL地址，指示客戶端是從哪個頁面跳轉過來的。例如，Referrer: <https://www.google.com/。>
- Accept-xxx：指示客戶端能夠處理的媒體類型，例如文本、圖片、音頻或視頻。例如，Accept-Language: en-US,en;q=0.9表示客戶端首選使用英文。
- host：這指示了請求要傳送到的目標主機位址。在這個例子中，請求被發送到了api.shop.com。
- Authorization：用於伺服器端身份驗證的憑證，通常用於保護伺服器上的資源。例如，Authorization: Bearer abcdef123456。
