# 發送請求 Request

---

跟寄信的概念是差不多的，都有一定的格式。

- start line
    - 一個 Http 方法
    - 請求目標(request target): 通常是一個Url
    - Http 版本
- Header: 額外的資訊，像是這個請求要發送到哪裡，或這個請求是從哪裡發送來的，又或著是我們發送的內容物(body)資料類型是哪一種
- Body: 我們要傳送過去的東西
